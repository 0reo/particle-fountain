#ifdef __APPLE__
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif

#include <stdlib.h>
#include <iostream>
#include "particle.h"
#include "prims.h"

using namespace std;

static int slices = 16;
static int stacks = 16;
Particle* particles[100000];
float eyes[3]={0.0, 5.0, -10.0};

/* GLUT callback Handlers */

const GLfloat light_ambient[]  = { 0.5f, 0.5f, 0.5f, 1.0f };
const GLfloat light_diffuse[]  = { 0.5f, 0.5f, 0.5f, 1.0f };
const GLfloat light_specular[] = { 0.0f, 1.0f, 0.5f, 1.0f };
const GLfloat light_position[] = { 2.0f, 5.0f, 5.0f, 0.0f };
//float light_pos[] = {5.0, 5.0, 5.0, 1.0};


materialStruct redPlastic =
{
    {0.3, 0.6, 1.0, 1.0},
    {1.0, 0.6, 1.0, 1.0},
    {0.0, 0.6, 0.6, 1.0},
    50.0f
};


void init(void)
{

    glViewport(0, 0, 300, 300);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();

    glOrtho(-300,300,-300,300,-300,300);
    gluPerspective(45.0f, 1.0f, 1.0f, 100.0f);


    glMatrixMode(GL_MODELVIEW);

    glEnable(GL_DEPTH_TEST);

    glClearColor(0,0,0,0);

    glLightfv(GL_LIGHT0, GL_AMBIENT,  light_ambient);
    glLightfv(GL_LIGHT0, GL_DIFFUSE,  light_diffuse);
    glLightfv(GL_LIGHT0, GL_SPECULAR, light_specular);
    glLightfv(GL_LIGHT0, GL_POSITION, light_position);

    for (int i = 0; i < 100000; i++)
    {
//        if (int(glutGet(GLUT_ELAPSED_TIME))%100 == 1)
//        {

        particles[i] = new Particle();
        particles[i]->setAge(double(rand()%100));
        //cout << particles[i]->getAge();
        particles[i]->setMaterial(&redPlastic);
//       }
    }



//    setMaterial(&redPlastic);

}

static void resize(int width, int height)
{
    const float ar = (float) width / (float) height;

    glViewport(0, 0, width, height);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(45.0f, 1.0f, 1.0f, 100.0f);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity() ;
}

static void display(void)
{
    const double t = glutGet(GLUT_ELAPSED_TIME) / 1000.0;
    const double a = t*90.0;

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);	// Clear The Screen And The Depth Buffer
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    //glLightfv(GL_LIGHT0, GL_POSITION, light_position);

    gluLookAt(eyes[0], eyes[1], eyes[2],//eyes
              0.0, 0.0, 0.0, //looking at
              0.0, 1.0, 0.0);//up direction


    glBegin(GL_POINTS);
    int c = 0;

    while (c < 100000)
    {
        //if (particles[c]->getAge() > 0.0)
        //glColor3d(double(rand()%10)/10.0, double(rand()%10)/10.0, double(rand()%10)/10.0);
        glVertex3d(particles[c]->getPosition().x,particles[c]->getPosition().y,particles[c]->getPosition().z);
        //particles[c]->setAge(glutGet(GLUT_ELAPSED_TIME)%10000);
        c++;
        //if (c >= 49999)
        //    c = 0;

        //cout << "t - " << int(glutGet(GLUT_ELAPSED_TIME))%10 << endl;

    }
    glEnd();
        float cen[2] = {0.0,0.0};
    float normals[][3]={{0,0,-1},{0,1,0},{-1,0,0},{1,0,0},{0,0,1},{0,-1,0}};
    prims *shape;
    shape = new prims();
    //glColor3d(0.0, 0.0, 1.0);
    shape->drawBox(cen, 4.0f, 0.5f, 1.0f, normals);

    glutSwapBuffers();
}


static void key(unsigned char key, int x, int y)
{
    switch (key)
    {
    case 27 :
    case 'q':
        exit(0);
        break;

    case '+':
        eyes[0]++;
        break;

    case '-':
        eyes[0]--;
        break;
    }

    glutPostRedisplay();
}

static void idle(void)
{
    double g = -0.29;
    for (int i = 0; i < 100000; i++)
    {
        if (particles[i]->getAge() >= 50.0)
        {
            particles[i]->~Particle();
            particles[i] = new Particle();
            particles[i]->setMaterial(&redPlastic);
            srand(glutGet(GLUT_ELAPSED_TIME)%100000);
            particles[i]->setAge(double(rand()%10-10));
            particles[i]->setSpeed((double(rand()%10)-5.0)/200.0, double(rand()%10+9)/8, (double(rand()%10)-5.0)/200.0);
        }
        else
        {
            particles[i]->setAge(particles[i]->getAge()+1);

            double y = 0.0;
            Vector p = particles[i]->getPosition();

            particles[i]->setSpeed(particles[i]->getSpeed().x, (g*particles[i]->getSpeed().y)+particles[i]->getSpeed().y, particles[i]->getSpeed().z);

            p.x-=particles[i]->getSpeed().x;//*particles[i]->getDirection().x;
            p.y+=particles[i]->getSpeed().y+(g/2);//*particles[i]->getDirection().y;
            if (particles[i]->getPosition().y <= -0.01)
            {
                p.y = 0.0;
            }
            p.z+=particles[i]->getSpeed().z;
            particles[i]->setPosition(p);
        }
    }
    //Vector p = particles[0]->getPosition();
    //cout << glutGet(GLUT_ELAPSED_TIME) << endl;
    glutPostRedisplay();

}


/* Program entry point */

int main(int argc, char *argv[])
{
    glutInit(&argc, argv);
    glutInitWindowSize(640,480);
    glutInitWindowPosition(10,10);
    glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH);

    glutCreateWindow("GLUT Shapes");

    glutReshapeFunc(resize);
    glutDisplayFunc(display);
    glutKeyboardFunc(key);
    glutIdleFunc(idle);

    //glClearColor(1,1,1,1);
    glEnable(GL_CULL_FACE);
    glCullFace(GL_BACK);

    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LESS);

    glEnable(GL_LIGHT0);
    glEnable(GL_NORMALIZE);
    glEnable(GL_COLOR_MATERIAL);
    glEnable(GL_LIGHTING);


    init();
    glutMainLoop();

    return EXIT_SUCCESS;
}
