#ifndef PARTICLE_H
#define PARTICLE_H

#ifdef __APPLE__
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif

typedef struct Vector
{
    double x, y,z;
}_Vector;

typedef struct materialStruct
{
    float ambient[4];
    float diffuse[4];
    float specular[4];
    float shininess;
} materialStruct;

class Particle
{
public:
    Particle();
    virtual ~Particle();
    Vector getPosition();
    void setPosition(double x, double y, double z);
    void setPosition(Vector p);
    Vector getDirection();
    void setDirection(double x, double y, double z);
    void setDirection(Vector d);
    Vector getSpeed();
    void setSpeed(double x, double y, double z);
    void setSpeed(Vector s);
    double getAge();
    void setAge(double a);
    void setMaterial(materialStruct *mat);
    materialStruct getMaterial();

protected:
private:
    Vector position;
    Vector direction;
    Vector speed;
    materialStruct material;
    double age;
};

#endif // PARTICLE_H

