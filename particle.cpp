#include "particle.h"

Particle::Particle()
{
    position.x = 0.0;
    position.y = 0.0;
    position.z = 0.0;

    speed.x = 0.0;
    speed.y = 0.2;
    speed.z = 0.0;
    for (int i = 0; i < 4; i++)
    {
        material.ambient[i]=0.3;
        material.diffuse[i]=1.0;
        material.specular[i]= 1.0;
    }
    material.shininess =32.0f;
    glPointSize(1);
    glEnable(GL_POINT_SMOOTH);
    age = 0.0;
}
Vector Particle::getPosition()
{
    return position;
}

void Particle::setPosition(double x, double y, double z)
{
    position.x = x;
    position.y = y;
    position.z = z;

}

void Particle::setPosition(Vector p)
{
    position = p;

}

Vector Particle::getDirection()
{
    return direction;
}

void Particle::setDirection(double x, double y, double z)
{
    direction.x = x;
    direction.y = y;
    direction.z = z;

}

void Particle::setDirection(Vector d)
{
    direction = d;
}

Vector Particle::getSpeed()
{
    return speed;
}

void Particle::setSpeed(double x, double y, double z)
{
    if (speed.x < 1.0 || speed.x > -1.0)
    speed.x = x;
    speed.y = y;
    if (speed.z < 1.0 || speed.z > -1.0)
    speed.z = z;

}

void Particle::setSpeed(Vector s)
{
    speed = s;
}

void Particle::setMaterial(materialStruct *mat)
{

    glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, mat->ambient);
    glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, mat->diffuse);
    glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, mat->specular);
    glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, mat->shininess);

}

materialStruct Particle::getMaterial()
{
        return material;
}

double Particle::getAge()
{
        return age;
}

void Particle::setAge(double a)
{
    age = a;
}

Particle::~Particle()
{
    //dtor
}
